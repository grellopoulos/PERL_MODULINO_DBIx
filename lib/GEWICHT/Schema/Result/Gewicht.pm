use utf8;
package GEWICHT::Schema::Result::Gewicht;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GEWICHT::Schema::Result::Gewicht

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<gewicht>

=cut

__PACKAGE__->table("gewicht");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 kilogramm

  data_type: 'real'
  is_nullable: 1

=head2 getraenk_einheiten

  data_type: 'integer'
  is_nullable: 1

=head2 speise_einheiten

  data_type: 'integer'
  is_nullable: 1

=head2 zeit

  data_type: 'date'
  default_value: DATETIME('NOW', 'LOCALTIME')
  is_nullable: 1

=head2 zeitstempel

  data_type: 'timestamp'
  default_value: strftime('%s', 'now')
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "kilogramm",
  { data_type => "real", is_nullable => 1 },
  "getraenk_einheiten",
  { data_type => "integer", is_nullable => 1 },
  "speise_einheiten",
  { data_type => "integer", is_nullable => 1 },
  "zeit",
  {
    data_type     => "date",
    default_value => \"DATETIME('NOW', 'LOCALTIME')",
    is_nullable   => 1,
  },
  "zeitstempel",
  {
    data_type     => "timestamp",
    default_value => \"strftime('%s', 'now')",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-09-06 14:56:58
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:n/QnHEMH0GEKG7GXHj87Rg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
