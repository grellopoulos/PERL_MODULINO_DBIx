Mo 19. Sep 15:07:02 CEST 2022

Kleine Einführung über die Verwendung von Modulen und
Modulinos unter Perl

Wenn Module einer Perl-Anwendung sich in verschiedenen
Verzeichnisse befinden, kann es notwendig werden,
Pfadanpassungen mittels FindBin vorzunehmen. Um dies
hier zu illustrieren, habe ich hier eine kleine
Beispielanwendung zusammengestellt.

Die Anwendung ist -- wie könnte es anders sein --
modular aufgebaut. Sie enthält die Module MOD1.pm,
MOD2.pm, MOD3.pm, MOD4.pm, den Ordner lib, der seinerseits
eine DBIx-Klasse enthält, die den Datenbankzugriff über
einen objektrelationalen Mapper ermöglicht, und eine
SQLite3-Beispieldatenbank (gewicht.db) sowie die
vorliegende Textdatei.

Bei MOD2.pm handelt es sich um ein so genanntes
Modulino, dass auch als Stand-alone-Skript aufgerufen
werden kann.

WICHTIG FÜR DAS FUNKTIONIEREN DER ANWENDUNG:

Die Module MOD3.pm und MOD4.pm müssen nach dem
Herunterladen an die für sie vorgesehenen Orte kopiert
werden, damit die Anwendung funktioniert: MOD3.pm muss
hierarchisch in einer Ebene über MODULINO, MOD4.pm zwei
Ebenen über MODULINO in einem eigenen Verzeichnis namens
MOD4 liegen -- der Einfachheit halber habe ich beides
in das Wurzelverzeichnis der Beispielanwendung kopiert.
Also bitte noch an die richtige Stelle kopieren oder
MOD2.pm entsprechend anpassen.

Weitere Dokumentation in MOD2.pm.

Bei Fragen: andreas.grell@grellopolis.de
