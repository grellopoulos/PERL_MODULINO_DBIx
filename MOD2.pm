package MOD2;

use strict;
use warnings;
use diagnostics;

# Mo 19. Sep 15:27:13 CEST 2022
# a.grell@wertgarantie.com
#
# Der erste Aufruf von FindBin bzw. $Bin
# ermöglicht es MOD2.pm, die notwendigen
# Module zu finden.

use FindBin qw( $Bin );
use lib "$Bin";

use MOD1;

# Der zweite und alle folgenden Aufrufe
# derselben Anwendung bedürfen für die volle
# Funktionalität der Funktion FindBin::again():
# Nur so ist gewährleistet, dass die entsprechenden
# Module überhaupt gefunden werden können.

use lib "$Bin/../";
FindBin::again();
use MOD3::MOD3;

use lib "$Bin/../../";
FindBin::again();
use MOD4::MOD4;

use lib "$Bin/lib";
FindBin::again();
use GEWICHT::Schema;

MOD2->run unless caller;

sub run {
    print "Dies ist ein TEST in MODULINO " . __PACKAGE__ . ".\n";
    print MOD1::test;
    print MOD3::test;
    print MOD4::test;
}

my $schema = GEWICHT::Schema->connect( 'dbi:SQLite:gewicht.db' )
    or die $!; 

my $rs = $schema->resultset('Gewicht');

for my $key ( $rs->all ){
    my $result = $key->{ _column_data };
    printf "ID: %d\n", $result->{ id };
    printf "\tKilogramm: %2.1f\n", $result->{ kilogramm };
    printf "\tSpeiseeinheiten: %d\n", $result->{ speise_einheiten };
    printf "\tGetränkeeinheiten: %d\n", $result->{ getraenk_einheiten };
    printf "\tZeit: %s\n", $result->{ zeit };
    printf "\tZeitstempel: %d\n", $result->{ zeitstempel };    
    print "\n";
}

print "\n";
print "=" x 100;
print "\n";

$rs = $schema->resultset('Gewicht')->find(16);

print $rs->get_column('kilogramm') . "\n";

print "\n";
print "=" x 100;
print "\n";

printf "ID %d\n", $rs->id;
printf "\tGewicht: %2.1f\n", $rs->kilogramm;
printf "\tSpeise: %d\n", $rs->speise_einheiten;
printf "\tGetränke: %d\n", $rs->getraenk_einheiten;
printf "\tZeit: %s\n", $rs->zeit;
printf "\tUNIX-Zeitstempel: %d\n", $rs->zeitstempel;

1;
